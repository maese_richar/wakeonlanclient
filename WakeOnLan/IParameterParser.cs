﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLan
{
    /// <summary>
    /// Parse the input parameters and obtain the MAC address.
    /// </summary>
    public interface IParameterParser
    {
        PhysicalAddress Parse(string[] args);
    }
}
