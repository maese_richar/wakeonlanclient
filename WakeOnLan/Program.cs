﻿using System;
using WakeOnLanClient;

namespace WakeOnLan
{
    class Program
    {
        private static IParameterParser _parser;
        private static IWakeOnLanClient _client;

        static Program()
        {
            _parser = new ParameterParser();
            var builder = new MagicPacketBuilder();
            _client = new WakeOnLanClient.WakeOnLanClient(builder);
        }
        static void Main(string[] args)
        {
            var address = _parser.Parse(args);
            Console.WriteLine($"Sending WoL packet to address {address}");
            _client.Wake(address);
        }
    }
}
