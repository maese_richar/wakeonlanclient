﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLan
{
    public class ParameterParser : IParameterParser
    {
        public PhysicalAddress Parse(string[] args)
        {
            if (args.Length != 1)
            {
                throw new ArgumentException("Wrong number of arguments!!");
            }

            return PhysicalAddress.Parse(args[0].ToUpper());
        }
    }
}
