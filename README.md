# WakeOnLanClient

This is a simple library to send WOL messages to a given MAC address.

It also comes with a CLI application that makes the call to the library.

There are samples of the use of the library in the test project.

## Usage of the CLI application

`
 WakeOnLan.exe <MAC ADDRESS>
`