﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLanClient
{
    /// <summary>
    /// A client to send WakeOnLan packets.
    /// </summary>
    // TODO: Offer an async version
    public interface IWakeOnLanClient
    {
        /// <summary>
        /// Send a packet to the provided mac address.
        /// </summary>
        /// <param name="macAddress">The MAC address fo the machine to be waked.</param>
        void Wake(PhysicalAddress macAddress);
    }
}
