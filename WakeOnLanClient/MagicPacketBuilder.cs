﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLanClient
{
    public class MagicPacketBuilder : IMagicPacketBuilder
    {
        public byte[] Build(PhysicalAddress macAddress)
        {
            if (macAddress == null)
            {
                throw new ArgumentNullException(nameof(macAddress));
            }

            var addressAsBytes = macAddress.GetAddressBytes();
            var addressLength = addressAsBytes.Length;
            var packet = new byte[102];

            for(var i = 0; i < 6; i++)
            {
                packet[i] = 0xFF;
            }

            for(var i = 0; i < 16; i++)
            {
                Array.Copy(addressAsBytes, 0, packet, 6 + (i * addressLength), addressLength);
            }

            return packet;
        }
    }
}
