﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace WakeOnLanClient
{
    public class WakeOnLanClient : IWakeOnLanClient
    {
        private readonly IMagicPacketBuilder _packetBuilder;

        public WakeOnLanClient(IMagicPacketBuilder packetBuilder)
        {
            _packetBuilder = packetBuilder ?? throw new ArgumentNullException(nameof(packetBuilder));
        }

        public void Wake(PhysicalAddress macAddress)
        {
            if (macAddress == null)
            {
                throw new ArgumentNullException(nameof(macAddress));
            }

            var packet = _packetBuilder.Build(macAddress);

            using (var client = new UdpClient(9))
            {
                client.Connect(IPAddress.Broadcast, 9);
                client.Send(packet, packet.Length);
            }
        }
    }
}
