﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLanClient
{
    /// <summary>
    /// Build a magic packet for the provided MAC address.
    /// </summary>
    public interface IMagicPacketBuilder
    {
        /// <summary>
        /// Build a magic packet for the provided MAC address.
        /// </summary>
        /// <param name="macAddress">The address used to generate the packet.</param>
        /// <returns>The magic packet content as byte array.</returns>
        byte[] Build(PhysicalAddress macAddress);
    }
}
