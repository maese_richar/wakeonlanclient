﻿using NUnit.Framework;
using System;

namespace WakeOnLan.Tests
{
    [TestFixture]
    public class ParameterParserTests
    {
        [Test]
        public void IfWeTryToParseMoreThanOneParameterThrowsArgumentException()
        {
            var parser = new ParameterParser();
            var args = new[] { "one", "two" };

            Assert.Throws<ArgumentException>(() => parser.Parse(args));
        }

        [Test]
        public void TheParsedAddressIsTheOneProvided()
        {
            var parser = new ParameterParser();
            var expectedAddress = "001422012345";
            var args = new[] { expectedAddress };

            var address = parser.Parse(args);

            Assert.AreEqual(expectedAddress, address.ToString());
        }

        [Test]
        public void TheParsedAddressIsTheOneProvidedEvenWithSeparators()
        {
            var parser = new ParameterParser();
            var expectedAddress = "001422012345";
            var args = new[] { "00-14-22-01-23-45" };

            var address = parser.Parse(args);

            Assert.AreEqual(expectedAddress, address.ToString());
        }
    }
}
