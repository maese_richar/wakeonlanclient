#tool "nuget:?package=NUnit.ConsoleRunner"
#tool "nuget:?package=OpenCover"
#tool "nuget:?package=ReportGenerator"

var target = Argument("target", "Test");
var apiKey = Argument("apiKey", (string)null);

Task("Build")
  .Does(() =>
{
  DotNetCoreBuild("./WakeOnLanClient.sln");
});

Task("Test")
  .IsDependentOn("Build")
  .Does(() =>
{
  DotNetCoreVSTest("./**/bin/**/*.Tests.dll");
});

Task("Coverage")
  .IsDependentOn("Build")
  .Does(() =>
  {
	var openCoverSettings = new OpenCoverSettings
    {
        OldStyle = true,
        //MergeOutput = true
    }
    .WithFilter("+[*]* -[moq*]*");
	OpenCover(tool => {
  tool.DotNetCoreVSTest("./**/bin/**/*.Tests.dll"); 
  },
  new FilePath("./result.xml"),
  openCoverSettings);

  ReportGenerator("./result.xml", "./report/");
  });

Task("Publish")
  .IsDependentOn("Test")
  .Does(() =>
  {
	  if (string.IsNullOrWhiteSpace(apiKey))
	  {
		  throw new ArgumentException("For the 'Publish' target you must provide an API key.");
      }

	  var package = GetFiles(@".\WakeOnLanClient\bin\Debug\WakeOnLanClient.*.nupkg");

	   NuGetPush(package, new NuGetPushSettings {
			 Source = "https://api.nuget.org/v3/index.json",
			 ApiKey = apiKey
		 });
  });

RunTarget(target);