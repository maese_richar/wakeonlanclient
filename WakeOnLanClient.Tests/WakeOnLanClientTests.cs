﻿using Moq;
using NUnit.Framework;
using System;
using System.Net.NetworkInformation;

namespace WakeOnLanClient.Tests
{
    [TestFixture]
    public class WakeOnLanClientTests
    {
        private Mock<IMagicPacketBuilder> _packetBuilder;

        [SetUp]
        public void SetUp()
        {
            _packetBuilder = new Mock<IMagicPacketBuilder>();
        }

        [Test]
        public void IfWeTryToWakeANullMACAddressThrowsArgumentNullException()
        {
            var client = BuildClient();

            Assert.Throws<ArgumentNullException>(() => client.Wake(null));
        }

        [Test]
        public void TheMagicPacketBuilderIsCalledWithTheCorrectMac()
        {
            var client = BuildClient();
            var macAddress = PhysicalAddress.Parse("00-14-22-01-23-45");

            client.Wake(macAddress);

            _packetBuilder.Verify(b => b.Build(macAddress), Times.Once);
        }

        private WakeOnLanClient BuildClient()
        {
            return new WakeOnLanClient(_packetBuilder.Object);
        }
    }
}
