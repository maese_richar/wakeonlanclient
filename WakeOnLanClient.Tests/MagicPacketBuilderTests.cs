﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace WakeOnLanClient.Tests
{
    [TestFixture]
    public class MagicPacketBuilderTests
    {
        [Test]
        public void IfWeTryToBuildAPacketForNullMacAddressThrowsArgumentNUllException()
        {
            var builder = new MagicPacketBuilder();

            Assert.Throws<ArgumentNullException>(() => builder.Build(null));
        }

        [Test]
        public void ThePacketHasALengthOf102Bytes()
        {
            var builder = new MagicPacketBuilder();
            var macAddress = PhysicalAddress.Parse("00-14-22-01-23-45");

            var packet = builder.Build(macAddress);

            Assert.AreEqual(102, packet.Length);
        }

        [Test]
        public void TheFirstSixBytesAreFF()
        {
            var builder = new MagicPacketBuilder();
            var macAddress = PhysicalAddress.Parse("00-14-22-01-23-45");

            var packet = builder.Build(macAddress);

            var head = packet.Take(6);
            Assert.IsTrue(head.All(b => b == 0xFF), "The first six bytes must be FF");
        }

        [Test]
        public void TheRestOfThePacketIsTheMacAddressRepeatedSixteenTimes()
        {
            var builder = new MagicPacketBuilder();
            var macAddress = PhysicalAddress.Parse("00-14-22-01-23-45");
            var expected = Enumerable.Repeat(new byte[] { 0x00, 0x14, 0x22, 0x01, 0x23, 0x45 }, 16)
                            .SelectMany(b => b)
                            .ToArray();

            var packet = builder.Build(macAddress);

            var rest = packet.Skip(6);
            CollectionAssert.AreEqual(expected, rest, "The rest of the packet must be the mac address repeated 16 times.");
        }
    }
}
